import React, { useState } from 'react';

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState('');
  const [showCompleted, setShowCompleted] = useState(true);

  const handleAddTodo = () => {
    if (!newTodo) return;
    setTodos([...todos, { task: newTodo, completed: false }]);
    setNewTodo('');
  }

  const handleToggleCompleted = () => {
    setShowCompleted(!showCompleted);
  }

  const handleToggleTodoCompleted = (index) => {
    const updatedTodos = [...todos];
    updatedTodos[index].completed = !updatedTodos[index].completed;
    setTodos(updatedTodos);
  }

  const filteredTodos = showCompleted
    ? todos
    : todos.filter(todo => !todo.completed);

  return (
    <div>
      <h1>Todo List</h1>
      <div>
        <label>
          <input
            type="checkbox"
            checked={showCompleted}
            onChange={handleToggleCompleted}
          />
          Show Completed
        </label>
      </div>
      <ul>
        {filteredTodos.map((todo, i) => (
          <li key={i}>
            <input
              type="checkbox"
              checked={todo.completed}
              onChange={() => handleToggleTodoCompleted(i)}
            />
            {todo.task}
          </li>
        ))}
      </ul>
      <div>
        <input
          type="text"
          value={newTodo}
          onChange={(e) => setNewTodo(e.target.value)}
        />
        <button onClick={handleAddTodo}>Add Todo</button>
      </div>
    </div>
  );
}

export default TodoList;